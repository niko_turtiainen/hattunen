import React from 'react'
import './App.css'
import Button from 'react-bootstrap/Button'

// destructuring props
const Feeding = ({ feeding, toggleFeeding }) => {

    // render feeding info
    const listOfFeeding = feeding.map(item => {
        return (
            <Button className="toggleBtn" variant={item.givenToday ? 'secondary' : null} onClick={() => { toggleFeeding(item.id) }} key={item.id}>
                <div className="top">
                    <div className="top-left">{item.shift}</div>
                    <div className="top-right">{item.type}</div>
                </div>
                <div className="time">{item.givenToday ? item.time : item.prevTime}</div>
            </Button>
        )
    })

    return (
        <div className="grid-container">
            {listOfFeeding}
        </div>

    )
}

export default Feeding
