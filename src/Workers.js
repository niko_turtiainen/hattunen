import React from 'react'
import './App.css'
import ListGroup from 'react-bootstrap/ListGroup'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Juuso from './icons/juuso.jpg'
import Niko from './icons/niko.jpg'
import Pauli from './icons/pauli.jpg'

// destructuring props
const Workers = ({ workers, toggleJob }) => {

    const listOfWorkers = workers.map(worker => {
        let image;

        // get correct image
        switch (worker.name) {
            case 'Niko':
                image = Niko
                break;
            case 'Juuso':
                image = Juuso
                break;
            case 'Pauli':
                image = Pauli
                break;

            default:
                break;
        }

        return (
            <ListGroup as={Row} horizontal sm="12" style={{ margin: 10, fontSize: 35 }} key={worker.id}>
                <ListGroup.Item sm="2" variant="dark">
                    <img src={image} alt="avatar"></img>
                </ListGroup.Item>
                <ListGroup.Item sm="4" variant="dark" as={Col}>{worker.name}</ListGroup.Item>
                <ListGroup.Item variant="dark" as={Col}>{worker.job}</ListGroup.Item>
            </ListGroup>
        )
    })

    return (
        listOfWorkers
    )
}

export default Workers