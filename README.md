# Hattunen
Web app for tracking cat's feeding and employee's shift

## Technology
- npm 6.13.7
- node v10.16.0

## How to install

Download project
<pre><code>niko@linux:~$ git clone https://gitlab.com/niko_turtiainen/hattunen.git
Cloning into 'hattunen'...
</code></pre>

Change folder
<pre><code>niko@linux:~$ cd hattunen
</code></pre>

Install needed dependencies with node
<pre><code>niko@linux:~/studies$ npm i
</code></pre>

Launch react app using npm start
<pre><code>niko@linux:~/hattunen$ npm start
</code></pre>
