import React, { Component } from 'react';
import Workers from './Workers';
import Feeding from './Feeding';
import 'bootstrap/dist/css/bootstrap.min.css';

class App extends Component {

  state = {
    workers: [
      { id: 1, name: 'Niko', done: false, color: "yellow", job: 'Roskat' },
      { id: 2, name: 'Juuso', done: false, color: "yellow", job: 'Tiskit' },
      { id: 3, name: 'Pauli', done: false, color: "yellow", job: 'Hattu WC' }
    ],
    feeding: [
      { id: 1, time: '--:--', prevTime: '8:55', givenToday: false, shift: 'aamu', type: 'märkäruoka', },
      { id: 2, time: '--:--', prevTime: '14:23', givenToday: false, shift: 'aamu', type: 'kuivaruoka', },
      { id: 3, time: '--:--', prevTime: '17:42', givenToday: false, shift: 'ilta', type: 'märkäruoka', },
      { id: 4, time: '--:--', prevTime: '20:57', givenToday: false, shift: 'ilta', type: 'kuivaruoka', }
    ],
    currentDay: null,
    newWeek: true,
    weekNumber: null
  }


  // set current day, start timer
  componentDidMount() {
    var d = new Date();
    this.setState({
      currentDay: d.getDate()
    })

    // run once in a minute
    setInterval(() => {
      this.timer();
    }, 60000);
  }

  // timer logic
  timer() {
    var d = new Date();
    var day = d.getDate();
    var hours = d.getHours();
    var weekDay = d.getDay();
    let newWeek = this.state.newWeek

    // change week on monday, reset boolean on sunday (to make it switch jobs only once)
    if (weekDay === 1 && newWeek) {
      this.setState({ newWeek: false })
      this.switchJobs();
    } else if (weekDay === 0) {
      this.setState({ newWeek: true })
    }

    // check if day has changed and the clock is over 2:00, change current day and feeding boolean
    if (day !== this.state.currentDay && hours >= 2) {
      // shallow (?) copy state object
      var feedingCopy = JSON.parse(JSON.stringify(this.state.feeding));

      // change givenToday to false, prevTime to time
      for (var j = 0; j < feedingCopy.length; j++) {
        feedingCopy[j].givenToday = false;
        if (feedingCopy[j].time) {
          feedingCopy[j].prevTime = feedingCopy[j].time;
        } else {
          feedingCopy[j].prevTime = '--:--'
        }
        feedingCopy[j].time = null
      }

      // update state
      this.setState({
        feeding: feedingCopy,
        currentDay: day
      })
    }
  }


  // when user press feeding button
  toggleFeeding = (id) => {
    // make shallow(?) copy
    var feedingCopy = JSON.parse(JSON.stringify(this.state.feeding));

    // calc time in correct format  
    var date = new Date();
    var hours = date.getHours();
    var minutes = date.getMinutes();
    if (minutes < 10) {
      minutes = '0' + minutes;
    };
    var time = hours + ':' + minutes;

    // check if has been given today
    if (!feedingCopy[id - 1].givenToday) {
      feedingCopy[id - 1].time = time;
    } else {
      feedingCopy[id - 1].time = null;
    }

    // toggle state
    feedingCopy[id - 1].givenToday = !feedingCopy[id - 1].givenToday;

    // update data
    this.setState({
      feeding: feedingCopy
    })
  }


  // when user press work button
  toggleJob = (id) => {
    let workersCopy = JSON.parse(JSON.stringify(this.state.workers));
    workersCopy[id - 1].done = !workersCopy[id - 1].done;
    if (workersCopy[id - 1].done) {
      workersCopy[id - 1].color = "lightgreen"
    } else {
      workersCopy[id - 1].color = "yellow"
    }

    this.setState({
      workers: workersCopy
    })
  }


  // when everyone has finished their jobs, give them new one for the next week
  switchJobs = () => {
    let store = this.state.workers[2].job;
    let workersCopy = JSON.parse(JSON.stringify(this.state.workers));

    workersCopy[2].job = workersCopy[1].job;
    workersCopy[1].job = workersCopy[0].job;
    workersCopy[0].job = store;

    for (var i = 0; i < workersCopy.length; i++) {
      workersCopy[i].done = false;
      workersCopy[i].color = "yellow";
    }
    this.setState({
      workers: workersCopy,
      newWeek: false
    })
  }

  render() {

    let date = new Date();

    // current day
    const day = date.getDate() + '.' + (date.getMonth() + 1);

    // calculate monday
    date.setDate(date.getDate() - date.getDay() + 1);
    let monday = date.getDate() + '.' + (date.getMonth() + 1)

    // calculate sunday
    date.setDate(date.getDate() + 6);
    let sunday = date.getDate() + '.' + (date.getMonth() + 1)

    // render this text
    const week = monday + ' - ' + sunday


    return (
      <div className="App">

        <h1>-Työvuorot-
          <div className="date">{week}</div>
        </h1>

        <Workers workers={this.state.workers} toggleJob={this.toggleJob} nextWeek={this.nextWeek} previousWeek={this.previoustWeek} />
        <br></br>

        <h1>-Hattusen ruokinta-
          <div className="date">{day}</div>
        </h1>

        <Feeding feeding={this.state.feeding} toggleFeeding={this.toggleFeeding} />

      </div>
    );
  }
}

export default App;
